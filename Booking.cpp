#include "booking.h"

Booking::Booking()
{

}

Booking::~Booking()
{

}

double Booking::getPrice(void)
{
    return this->price;
}

void Booking::setTravelId(long travelId)
{
    this->travelId = travelId;
}

long Booking::getId(void)
{
    return this->id;
}

long Booking::getTravelId() const
{
    return travelId;
}

QDate Booking::getFromDate() const
{
    return fromDate;
}

QDate Booking::getToDate() const
{
    return toDate;
}
