#ifndef BOOKING_H
#define BOOKING_H
#include <string>
#include <QDate>

using namespace std;

class Booking
{
public:
    Booking();
    virtual ~Booking();
    double getPrice();
    void setTravelId(long);
    long getId();
    long getTravelId() const;
    QDate getFromDate() const;
    QDate getToDate() const;
    virtual string showDetails()=0;

protected:
    long id;
    double price;
    long travelId;
    QDate fromDate;
    QDate toDate;
};

#endif // BOOKING_H
