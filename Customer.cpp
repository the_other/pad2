#include "customer.h"

Customer::Customer(long id, string name)
{
    this->id = id;
    this->name = name;
}

Customer::~Customer()
{

}

void Customer::addTravel(Travel* travel)
{
    this->travelList.push_back(travel);
}

long Customer::getId() const
{
    return this->id;
}

string Customer::getName() const
{
    return name;
}
