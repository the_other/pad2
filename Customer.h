#ifndef CUSTOMER_H
#define CUSTOMER_H
#include "travel.h"
#include <string>
#include <vector>

using namespace std;

class Customer
{
public:
    Customer(long, string);
    ~Customer();
    void addTravel(Travel*);
    long getId() const;
    string getName() const;

private:
    long id;
    string name;
    vector<Travel*> travelList;
};

#endif // CUSTOMER_H
