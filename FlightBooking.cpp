#include "flightbooking.h"

FlightBooking::FlightBooking(long id, double price, QDate toDate, QDate fromDate, long travelId, string fromDest, string toDest, string airline, char seatPref)
{
    this->id = id;
    this->price = price;
    this->travelId = travelId;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->fromDest = fromDest;
    this->toDest = toDest;
    this->airline = airline;
    this->seatPref = seatPref;
}

FlightBooking::~FlightBooking()
{

}

char FlightBooking::getSeatPref() const
{
    return seatPref;
}

string FlightBooking::showDetails()
{
    return (seatPref == 'A') ? "Gang" : "Fenster";
}

string FlightBooking::getFromDest() const
{
    return fromDest;
}

string FlightBooking::getToDest() const
{
    return toDest;
}

string FlightBooking::getAirline() const
{
    return airline;
}
