#ifndef FLIGHTBOOKING_H
#define FLIGHTBOOKING_H
#include "booking.h"

class FlightBooking:
    public Booking
{
public:
    FlightBooking(long, double, QDate, QDate, long, string, string, string, char);
    ~FlightBooking();
    string showDetails();

    string getFromDest() const;
    string getToDest() const;
    string getAirline() const;
    char getSeatPref() const;

private:
    string fromDest;
    string toDest;
    string airline;
    char seatPref;
};

#endif // FLIGHTBOOKING_H
