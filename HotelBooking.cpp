#include "hotelbooking.h"

HotelBooking::HotelBooking(long id, double price, QDate fromDate, QDate toDate, long travelId, string hotel, string town, bool smoke)
{
    this->id = id;
    this->price = price;
    this->travelId = travelId;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->hotel = hotel;
    this->town = town;
    this->smoke = smoke;
}

HotelBooking::~HotelBooking()
{

}


string HotelBooking::showDetails()
{
    bool smoke = this->getSmoke();
    string strsmoke;
    if (smoke)
    {
        strsmoke = "true";
    }
    else
    {
        strsmoke = "false";
    }
    return strsmoke;
}

string HotelBooking::getHotel() const
{
    return hotel;
}

string HotelBooking::getTown() const
{
    return town;
}

bool HotelBooking::getSmoke() const
{
    return smoke;
}

void HotelBooking::setSmoke(bool value)
{
    smoke = value;
}
