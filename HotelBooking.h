#ifndef HOTELBOOKING_H
#define HOTELBOOKING_H
#include "booking.h"

class HotelBooking:
    public Booking
{
public:
    HotelBooking(long, double, QDate, QDate, long, string, string, bool);
    ~HotelBooking();
    string showDetails();

    string getHotel() const;
    string getTown() const;

    bool getSmoke() const;
    void setSmoke(bool value);

private:
    string hotel;
    string town;
    bool smoke;
};

#endif // HOTELBOOKING_H
