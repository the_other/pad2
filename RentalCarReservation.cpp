#include "rentalcarreservation.h"

RentalCarReservation::RentalCarReservation(long id, double price, QDate fromDate, QDate toDate, long travelId, string pickupLocation, string returnLocation, string company, string insuranceType)
{
    this->id = id;
    this->price = price;
    this->travelId = travelId;
    this->fromDate = fromDate;
    this->toDate = toDate;
    this->pickupLocation = pickupLocation;
    this->returnLocation = returnLocation;
    this->company = company;
    this->insuranceType = insuranceType;
}

RentalCarReservation::~RentalCarReservation()
{

}

string RentalCarReservation::showDetails()
{
    return insuranceType;
}

string RentalCarReservation::getPickupLocation() const
{
    return pickupLocation;
}

string RentalCarReservation::getReturnLocation() const
{
    return returnLocation;
}

string RentalCarReservation::getCompany() const
{
    return company;
}
