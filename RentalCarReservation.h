#ifndef RENTALCARRESERVATION_H
#define RENTALCARRESERVATION_H
#include "booking.h"

class RentalCarReservation:
    public Booking
{
public:
    RentalCarReservation(long, double, QDate, QDate, long, string, string, string, string);
    ~RentalCarReservation();
    string showDetails();

    string getPickupLocation() const;
    string getReturnLocation() const;
    string getCompany() const;

private:
    string pickupLocation;
    string returnLocation;
    string company;
    string insuranceType;
};

#endif // RENTALCARRESERVATION_H
