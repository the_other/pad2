#include "travel.h"

Travel::Travel(long id, long customerId)
{
    this->id = id;
    this->customerId = customerId;
}

Travel::~Travel()
{

}

void Travel::addBooking(Booking* booking)
{
    this->travelBookings.push_back(booking);
}

void Travel::setCustomerId(long customerId)
{
    this->customerId = customerId;
}

long Travel::getId() const
{
    return this->id;
}

long Travel::getCustomerId() const
{
    return customerId;
}
