#ifndef TRAVEL_H
#define TRAVEL_H
#include <vector>
#include "booking.h"

class Travel
{
public:
    Travel(long, long);
    ~Travel();
    void addBooking(Booking*);
    void setCustomerId(long);
    long getId() const;
    long getCustomerId() const;

private:
    long id;
    long customerId;
    vector<Booking*> travelBookings;
};

#endif // TRAVEL_H
