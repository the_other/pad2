#include "travelagency.h"
#include "flightbooking.h"
#include "rentalcarreservation.h"
#include "hotelbooking.h"

TravelAgency::TravelAgency(QObject* parent): QObject(parent)
{
}

TravelAgency::~TravelAgency()
{
    for (unsigned int i=0; i<this->allBookings.size(); i++)
    {
        delete allBookings[i];
    }
    for (unsigned int i=0; i<this->allCustomers.size(); i++)
    {
        delete allCustomers[i];
    }
    for (unsigned int i=0; i<this->allTravels.size(); i++)
    {
        delete allTravels[i];
    }
}

void TravelAgency::readFile(string filepath)
{
    ifstream quelle;
    quelle.open(filepath, ios::in);

    if (quelle.is_open()) {

    while (!quelle.eof()) {
        string zeile;
        getline(quelle, zeile);

        stringstream zeilenstream;
        zeilenstream << zeile;

        string attribut;
        vector <string> attribute;

        for(int i = 0; !zeilenstream.eof(); i++){
            getline(zeilenstream, attribut, '|');
            attribute.push_back(attribut);
        }

        Booking* booking = nullptr;
        QDate d1, d2;
        d1.setDate(stoi(attribute.at(3).substr(0,4)), stoi(attribute.at(3).substr(4,2)), stoi(attribute.at(3).substr(6,2)));
        d1.setDate(stoi(attribute.at(4).substr(0,4)), stoi(attribute.at(4).substr(4,2)), stoi(attribute.at(4).substr(6,2)));

        switch (attribute.at(0)[0]){
            case 'H':
                booking = new HotelBooking(stol(attribute.at(1)), stod(attribute.at(2)), d1, d2,
                                           stol(attribute.at(5)), attribute.at(8), attribute.at(9), stoi(attribute.at(10)));
            break;

            case 'F':
                booking = new FlightBooking(stol(attribute.at(1)), stod(attribute.at(2)), d1, d2,
                                            stol(attribute.at(5)), attribute.at(8), attribute.at(9), attribute.at(10), attribute.at(11).at(0));
            break;

            case 'R':
                booking = new RentalCarReservation(stol(attribute.at(1)), stod(attribute.at(2)), d1, d2,
                                                   stol(attribute.at(5)), attribute.at(8), attribute.at(9), attribute.at(10), attribute.at(11));
            break;

        }

        allBookings.push_back(booking);

        Customer* customer = findCustomer(stol(attribute.at(6)));
        if(!customer){
            customer = new Customer(stol(attribute.at(6)), attribute.at(7));
            allCustomers.push_back(customer);
        }

        Travel* travel = findTravel(stol(attribute.at(5)));
        if(!travel){
            travel = new Travel(stol(attribute.at(5)), stol(attribute.at(6)));
            allTravels.push_back(travel);
        }

        travel->addBooking(booking);
        customer->addTravel(travel);

    }
        emit(successBox(allBookings.size(), allTravels.size(), allCustomers.size(), getTotalValue()));
    }
    else {
        emit(errorBox(filepath));
    }
    quelle.close();

}

double TravelAgency::getTotalValue()
{
    double total = 0.0;
    for (unsigned int i = 0; i<allBookings.size(); i++)
    {
        total += allBookings[i]->getPrice();
    }
    return total;
}

Booking* TravelAgency::findBooking(long id)
{
    for (unsigned int i=0; i<allBookings.size(); i++) {
        if (allBookings[i]->getId() == id) {
            return allBookings[i];
        }
    }
    return nullptr;
}

Customer* TravelAgency::findCustomer(long id)
{
    for (unsigned int i=0; i<allCustomers.size(); i++) {
        if (allCustomers[i]->getId() == id) {
            return allCustomers[i];
        }
    }
    return nullptr;
}

Travel* TravelAgency::findTravel(long id)
{
    for (unsigned int i=0; i<allTravels.size(); i++) {
        if (allTravels[i]->getId() == id) {
            return allTravels[i];
        }
    }
    return nullptr;
}

void TravelAgency::showData()
{
    long ID;
    double price;
    string name;

    for (unsigned int i=0; i<allBookings.size(); i++)
    {
        ID = allBookings[i]->getId();
        price = allBookings[i]->getPrice();
        name = findCustomer(findTravel(allBookings[i]->getTravelId())->getCustomerId())->getName();
        emit(newline(ID, price, name, static_cast<int>(i)));
    }
}

void TravelAgency::getDetails(int i)
{

    long ID = allBookings.at(i)->getId();
    QDate fromDate = allBookings.at(i)->getFromDate();
    QDate toDate = allBookings.at(i)->getToDate();
    double price = allBookings.at(i)->getPrice();
    string name = findCustomer(findTravel(allBookings.at(i)->getTravelId())->getCustomerId())->getName();


    if (dynamic_cast<HotelBooking*>(allBookings.at(i)) != nullptr)
    {
        string hotel = dynamic_cast<HotelBooking*>(allBookings.at(i))->getHotel();
        string town = dynamic_cast<HotelBooking*>(allBookings.at(i))->getTown();
        string smoke = dynamic_cast<HotelBooking*>(allBookings.at(i))->showDetails();
        emit(detailsH(ID, fromDate, toDate, price, name, hotel, town, smoke));
    } else if (dynamic_cast<FlightBooking*>(allBookings.at(i)) != nullptr)
    {
        string fromDest = dynamic_cast<FlightBooking*>(allBookings.at(i))->getFromDest();
        string toDest = dynamic_cast<FlightBooking*>(allBookings.at(i))->getToDest();
        string airline = dynamic_cast<FlightBooking*>(allBookings.at(i))->getAirline();
        string seatPref = dynamic_cast<FlightBooking*>(allBookings.at(i))->showDetails();
        emit(detailsF(ID, fromDate, toDate, price, name, fromDest, toDest, airline, seatPref));
    } else if  (dynamic_cast<RentalCarReservation*>(allBookings.at(i)) != nullptr)
    {
        string pickup = dynamic_cast<RentalCarReservation*>(allBookings.at(i))->getPickupLocation();
        string return_ = dynamic_cast<RentalCarReservation*>(allBookings.at(i))->getReturnLocation();
        string company = dynamic_cast<RentalCarReservation*>(allBookings.at(i))->getCompany();
        string insurance = dynamic_cast<RentalCarReservation*>(allBookings.at(i))->showDetails();
        emit(detailsR(ID, fromDate, toDate, price, name, pickup, return_, company, insurance));
    }

    else {
        throw "getDetails mit i="+to_string(i)+"nicht gefunden";
    }
}

double TravelAgency::getDist(string stra, string strb, bool case_sensitive)
{
    string a = stra;
    string b = strb;

    if (!case_sensitive) {
        transform(a.begin(), a.end(), a.begin(), ::toupper);
        transform(b.begin(), b.end(), b.begin(), ::toupper);
    }
    int len_a = a.length();
    int len_b = b.length();

    int mat[len_a+1][len_b+1];
    mat[0][0] = 0;
    for (int x=1; x<=len_a; x++) { mat[x][0] = x; }
    for (int y=1; y<=len_b; y++) { mat[0][y] = y; }

    vector<int> lst;
    for (int x=1; x<=len_a; x++)
    {
        for (int y=1; y<=len_b; y++)
        {
            lst.clear();
            if (a[x-1]==b[y-1]) {
                lst.push_back(mat[x-1][y-1]);
            }
            else {
                lst.push_back(mat[x-1][y-1]+1);
            }
            lst.push_back(mat[x][y-1]+1);
            lst.push_back(mat[x-1][y]+1);


            mat[x][y] = *(min_element(lst.begin(), lst.end()));
        }
    }

    return mat[len_a][len_b]/(static_cast<double>(len_a+len_b));
}

void TravelAgency::search(string term, bool case_sensitive)
{
    struct Item
    {
        long ID;
        double dist;
    };

    vector<Item> unsorted;

    for (unsigned int i=0; i<allCustomers.size(); i++)
    {
        unsorted.push_back(Item());
        unsorted[i].ID = allCustomers[i]->getId();
        unsorted[i].dist = getDist(term, allCustomers[i]->getName(), case_sensitive);
    }

    vector<Item> sorted;


    int smallestIndex;
    for (unsigned int i=0; i<unsorted.size(); i++)
    {
        smallestIndex = 0;
        for (unsigned int j=0; j<unsorted.size(); j++)
        {
            smallestIndex = (unsorted[j].dist < unsorted[smallestIndex].dist) ? j : smallestIndex;
        }
        sorted.push_back(unsorted[smallestIndex]);
        unsorted[smallestIndex].dist = 1;
    }

    for (unsigned int i=0; i<sorted.size(); i++)
    {
        emit(newSearchResult(sorted[i].ID, findCustomer(sorted[i].ID)->getName()));
    }
}

