#ifndef TRAVELAGENCY_H
#define TRAVELAGENCY_H
#include <vector>
#include <string>
#include <QObject>
#include <sstream>
#include <fstream>
#include "booking.h"
#include "customer.h"
#include "travel.h"
#include <algorithm>
#include <QMessageBox>

using namespace std;

class TravelAgency: public QObject
{
    Q_OBJECT
public:
    TravelAgency(QObject* parent);
    ~TravelAgency();
    void readFile(string);
    double getTotalValue();
    Booking* findBooking(long);
    Customer* findCustomer(long);
    Travel* findTravel(long);
    void showData();
    void getDetails(int i);
    double getDist(string stra, string strb, bool case_sensitive);

signals:
    void newline(long id, double price, string name, int index);
    void detailsF(long id, QDate fromDate, QDate toDate, double, string, string fromDest, string toDest, string airline, string seatPref);
    void detailsR(long id, QDate fromDate, QDate toDate, double, string, string pickup, string return_, string company, string insurance);
    void detailsH(long id, QDate fromDate, QDate toDate, double, string, string hotel, string town, string smoke);
    void successBox(int bookings, int travels, int customers, double value);
    void errorBox(string filepath);
    void newSearchResult(long ID, string name);

public slots:
    void search(string term, bool case_sensitive);

private:
    vector<Booking*> allBookings;
    vector<Customer*> allCustomers;
    vector<Travel*> allTravels;
};

#endif // TRAVELAGENCY_H
