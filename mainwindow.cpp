#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList sl = QStringList();
    sl << "Buchungs ID" << "Preis" << "Kunde";
    ui->tableWidget->setHorizontalHeaderLabels(sl);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    sl.clear();
    sl << "ID" << "Kunde";
    ui->tableCustomers->setHorizontalHeaderLabels(sl);
    ui->tableCustomers->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ta = new TravelAgency(this);

    connect(ui->actionDatei_einlesen, SIGNAL(triggered(bool)),this,SLOT(on_actionDateiEinlesen_clicked()));
    connect(ui->actionBuchungen_anzeigen, SIGNAL(triggered(bool)),this,SLOT(on_actionBuchungenAnzeigen_clicked()));
    connect(ui->actionProgramm_beenden, SIGNAL(triggered(bool)),this,SLOT(on_actionProgrammBeenden_clicked()));
    connect(ta, &TravelAgency::newline, this, &MainWindow::newline);
    connect(ta, &TravelAgency::detailsF, this, &MainWindow::showDetailsF);
    connect(ta, &TravelAgency::detailsR, this, &MainWindow::showDetailsR);
    connect(ta, &TravelAgency::detailsH, this, &MainWindow::showDetailsH);
    connect(ta, &TravelAgency::successBox, this, &MainWindow::successBox);
    connect(ta, &TravelAgency::errorBox, this, &MainWindow::errorBox);
    connect(this, &MainWindow::search, ta, &TravelAgency::search);
    connect(ta, &TravelAgency::newSearchResult, this, &MainWindow::newSearchLine);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete ta;
}



void MainWindow::on_actionDateiEinlesen_clicked()
{
    QString filepath = QFileDialog::getOpenFileName(this, "Datei auswählen", "/", "Text files (*.txt)");
    if (filepath != NULL)
    {
        ta->readFile(filepath.toStdString());

    }
}

void MainWindow::on_actionBuchungenAnzeigen_clicked()
{    
    ui->tableWidget->setRowCount(0);
    ta->showData();
    ui->searchBar->setEnabled(true);
    ui->buttonSearch->setEnabled(true);
    ui->checkCaseSensitive->setEnabled(true);
    ui->tabWidget->setEnabled(true);

}

void MainWindow::on_actionProgrammBeenden_clicked()
{
    QCoreApplication::quit();
}

void MainWindow::newline(long ID, double price, string name, int index)
{
    ui->tableWidget->insertRow(index);
    ui->tableWidget->setItem(index, 0, new QTableWidgetItem(QString::number(ID)));
    ui->tableWidget->setItem(index, 1, new QTableWidgetItem(QString::number(price)));
    ui->tableWidget->setItem(index, 2, new QTableWidgetItem(QString::fromStdString(name)));
}

void MainWindow::tableDoubleClicked(QTableWidgetItem* item)
{
    ta->getDetails(item->row());
}

void MainWindow::successBox(int bookings, int travels, int customers, double value)
{
    stringstream ss;
    ss << to_string(bookings) << " Buchungen, "
              << to_string(travels) << " Reisen und "
              << to_string(customers) << " Kunden eingelesen,\nGesamtkosten von "
              << to_string(value);
    QMessageBox::information(this, "Einleseergebnis", ss.str().c_str(), QMessageBox::Ok);
}

void MainWindow::errorBox(string filepath)
{
    stringstream ss;
    ss << "Fehlen beim öffnen von" << endl << filepath;
    QMessageBox::critical(this, "Fehler", ss.str().c_str(), QMessageBox::Ok);
}

void MainWindow::showBasics(long ID, QDate fromDate, QDate toDate, double price, string name)
{
    ui->boxCalendar->setTitle(QString("Booking ID: ")+QString::number(ID));
    ui->calendarFrom->setSelectedDate(fromDate);
    ui->calendarTo->setSelectedDate(toDate);
    ui->lblPrice->setText(QString("Preis: ")+QString::number(price));
    ui->lblCustomer->setText(QString("Kunde: ")+QString::fromStdString(name));
}

void MainWindow::showDetailsF(long ID, QDate fromDate, QDate toDate, double price, string name, string fromDest, string toDest, string airline, string seatPref)
{
    showBasics(ID, fromDate, toDate, price, name);
    ui->lblType->setText("<b>Flight Booking</b>");
    ui->lblFlightFrom->setText(QString("Abflug von: ")+QString::fromStdString(fromDest));
    ui->lblFlightTo->setText(QString("Ankunft in: ")+QString::fromStdString(toDest));
    ui->lblFlightAirline->setText(QString("Airline: ")+QString::fromStdString(airline));
    ui->lblFlightSeatPref->setText(QString("Gewünschter Platz: ")+QString::fromStdString(seatPref));
    ui->detailsWidget->setCurrentIndex(0);
}

void MainWindow::showDetailsR(long ID, QDate fromDate, QDate toDate, double price, string name, string pickup, string return_, string company, string insurance)
{
    showBasics(ID, fromDate, toDate, price, name);
    ui->lblType->setText("<b>Rental Car Reservation</b>");
    ui->lblRentalPickup->setText(QString("Abholort: ")+QString::fromStdString(pickup));
    ui->lblRentalReturn->setText(QString("Abstellort: ")+QString::fromStdString(return_));
    ui->lblRentalCompany->setText(QString("Firma: ")+QString::fromStdString(company));
    ui->lblRentalInsurance->setText(QString("Versicherung: ")+QString::fromStdString(insurance));
    ui->detailsWidget->setCurrentIndex(1);
}

void MainWindow::showDetailsH(long ID, QDate fromDate, QDate toDate, double price, string name, string hotel, string town, string smoke)
{
    showBasics(ID, fromDate, toDate, price, name);
    ui->lblType->setText("<b>Hotel Booking</b>");
    ui->lblHotelHotel->setText(QString("Hotel: ")+QString::fromStdString(hotel));
    ui->lblHotelTown->setText(QString("Stadt: ")+QString::fromStdString(town));
    ui->lblHotelSmoke->setText(QString("Raucherzimmer: ")+QString::fromStdString(smoke));
    ui->detailsWidget->setCurrentIndex(2);
}

void MainWindow::on_searchBar_returnPressed()
{
    on_buttonSearch_clicked();
}

void MainWindow::on_buttonSearch_clicked()
{
    ui->search->setEnabled(true);

    if (!ui->searchBar->text().trimmed().isEmpty()) {
        ui->tableCustomers->setRowCount(0);
        emit(search(ui->searchBar->text().toLocal8Bit().constData(), ui->checkCaseSensitive->isChecked()));
        ui->tabWidget->setCurrentWidget(ui->search);
    }
}

void MainWindow::newSearchLine(long ID, string name)
{
    int index = ui->tableCustomers->rowCount();
    ui->tableCustomers->insertRow(index);
    ui->tableCustomers->setItem(index, 0, new QTableWidgetItem(QString::number(ID)));
    ui->tableCustomers->setItem(index, 1, new QTableWidgetItem(QString::fromStdString(name)));
}

