#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "flightbooking.h"
#include "rentalcarreservation.h"
#include "hotelbooking.h"
#include "travelagency.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QString>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QTableWidgetItem>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool readFile(string filepath);
    void addLineTable();

private:
    Ui::MainWindow *ui;
    TravelAgency* ta;

signals:
    void search(string term, bool case_sensitive);

private slots:
    void on_actionDateiEinlesen_clicked();
    void on_actionBuchungenAnzeigen_clicked();
    void on_actionProgrammBeenden_clicked();
    void newline(long ID, double price, string name, int index);
    void showBasics(long ID, QDate fromDate, QDate toDate, double, string);
    void showDetailsF(long ID, QDate fromDate, QDate toDate, double, string, string fromDest, string toDest, string airline, string seatPref);
    void showDetailsR(long ID, QDate fromDate, QDate toDate, double, string, string pickup, string return_, string company, string insurance);
    void showDetailsH(long ID, QDate fromDate, QDate toDate, double, string, string hotel, string town, string smoke);

    void on_searchBar_returnPressed();
    void on_buttonSearch_clicked();
    void newSearchLine(long ID, string name);

    void tableDoubleClicked(QTableWidgetItem* item);
    void successBox(int bookings, int travels, int customers, double value);
    void errorBox(string filepath);

};

#endif // MAINWINDOW_H
